#!/usr/bin/env bash

set -euo pipefail

cd /app

mkdir --parents t/build

touch t/build/foo1
ls -al t/build/foo1

# invalid date
#touch -d 2020-01-01T12:13:14 t/build/foo2
#ls -al t/build/foo2

# invalid date
#touch -d 2020-01-01-12:13:14 t/build/foo3
#ls -al t/build/foo3

touch -d 2020.01.01-12:13:14 t/build/foo4
ls -al t/build/foo4

touch -h

