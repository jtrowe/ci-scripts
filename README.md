# ci-scripts

Some resuable scripts for ci.

## Notes

### DateTimes

Probably try to deal with datetimes internally in whatever the local
timezone is.  The busybox commands are very limited in the date formats
they take.  And it is very hard to recalculate timezones.
So only produce UTC dates from local timezone dates; and only
when needed.

Below might be b/c system timezone is UTC.

touch assumes the input date is UTC.
Input date format like "1995.07.04-12:00:00".

date -r $file also outputs time as UTC.
Input date format like "1995.07.04-12:00:00".

