# .project-info.sh

# Contains project information as shell variables.

export PROJECT_AUTHOR="Joshua T. Rowe <jrowe@jrowe.org>"
export PROJECT_DESCRIPTION="Some resuable scripts for continuous integration."
export PROJECT_LICENSE=""
export PROJECT_NAME="ci-scripts"
export PROJECT_REPOSITORY="http://gitlab.com/jtrowe/ci-scripts.git"
export PROJECT_TITLE="ci-scripts"
export PROJECT_URL="http://gitlab.com/jtrowe/ci-scripts"
export PROJECT_VENDOR="jrowe.org"
export PROJECT_VERSION="1.1"

